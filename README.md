Nama : Arnoldy Fatwa Rahmadin

NIM : 222011330

Kelas : 3SI3

Praktikum Pemrograman Platform Khusus Pertemuan 6

[LINK DEMONSTRASI]

https://drive.google.com/file/d/1J-DCNreIbUxqVy-aHRwTThvRge53EUVp/view?usp=sharing

1. Halaman Home Page
![home](/dokumentasi/PPK-Praktikum%206_1.png)

2. Halaman Login
![login](/dokumentasi/PPK-Praktikum%206_2.png)

3. Halaman Register
![register](/dokumentasi/PPK-Praktikum%206_3.png)

4. Halaman Profile Setelah Melakukan Register
![profile-register](/dokumentasi/PPK-Praktikum%206_4.png)

5. Halaman Profile Saat Mencoba Sign-in With Google
![profile-signin](/dokumentasi/PPK-Praktikum%206_5.png)



